﻿using System;

namespace ex_19
{
    class Program
    {
        static void Main(string[] args)
        {
            // --------------------------------------------
            // Exercise #19 - using a little maths
            // --------------------------------------------
            // The unicode for superscript is \xB2 (where 2 will print the superscript)
            Console.WriteLine($"a) (6+7) * (3-2) = {(6+7) * (3-2)}");
            Console.WriteLine($"b) (6 * 7) + (3 * 2) = {(6 * 7) + (3 * 2)}");
            Console.WriteLine($"c) (6 * 7) + 3 * 2 = {(6 * 7) + 3 * 2}");
            Console.WriteLine($"d) (3 * 2) + 6 * 7 = {(3 * 2) + 6 * 7}");
            Console.WriteLine($"e) (3 * 2) + 7 * 6 / 2 = {(3 * 2) + 7 * 6 / 2}");
            Console.WriteLine($"f) 6 + 7 * 3 - 2 = {6 + 7 * 3 - 2}");
            Console.WriteLine($"g) 3 * 2 + (3 * 2) = {3 * 2 + (3 * 2)}");
            Console.WriteLine($"h) (6 * 7) * 7 + 6 = {(6 * 7) * 7 + 6}");
            Console.WriteLine($"i) (2 * 2) + 2 * 2 = {(2 * 2) + 2 * 2}");
            Console.WriteLine($"j) 3 * 3 + (3 * 3) = {3 * 3 + (3 * 3)}");
            Console.WriteLine($"k) (6\xB2 + 7) * 3 + 2 = {(Math.Pow(6,2) + 7) * 3 + 2}");
            Console.WriteLine($"l) (3 * 2) + 3\xB2 * 2 = {(3 * 2) + Math.Pow(3,2) * 2}");
            Console.WriteLine($"m) (6 * (7 + 7)) / 6 = {(6 * (7 + 7)) / 6}");
            Console.WriteLine($"n) ((2 + 2) + (2 * 2)) = {((2 + 2) + (2 * 2))}");
            Console.WriteLine($"o) 4 * 4 + (3\xB2 * 3\xB2) = {4 * 4 + (Math.Pow(3,2) * Math.Pow(3,2))}");
        }
    }
}
